const check_aqi_cb = (callback) => {
  axios.get("https://backend.airdeveloppa.services/1/deviceinfo/b27fb2a5-89ce-4613-9239-356f0fe7ddf3").then((response) => {
    if (response.data !== undefined && response.data !== null) {
      if (response.data['meta'] !== undefined && response.data['meta'] !== null) {
        if (response.data['meta']['status'] !== undefined && response.data['meta']['status'] !== null) {
          if (response.data['meta']['status'] === 200) {
            if (response.data['result'] !== undefined && response.data['result'] !== null) {
              if (response.data['result'].length === 1) {
                console.log("Showing device");
                if (response.data['result'][0]['devices'] !== undefined && response.data['result'][0]['devices'] !== null) {
                  console.log("Showing device as an AQI");
                  var AQIDefinition = "Undefined";
                  if (parseInt(response.data['result'][0]['devices'][0]['AQI']) <= 33) {
                    AQIDefinition = "<font color='#208000'>Very Good</font>";
                  } else if (parseInt(response.data['result'][0]['devices'][0]['AQI']) > 33 && parseInt(response.data['result'][0]['devices'][0]['AQI']) <= 66) {
                    AQIDefinition = "<font color='#33cc00'>Good</font>";
                  } else if (parseInt(response.data['result'][0]['devices'][0]['AQI']) > 66 && parseInt(response.data['result'][0]['devices'][0]['AQI']) <= 99) {
                    AQIDefinition = "<font color='#ffff00'>Fair</font>";
                  } else if (parseInt(response.data['result'][0]['devices'][0]['AQI']) > 99 && parseInt(response.data['result'][0]['devices'][0]['AQI']) <= 149) {
                    AQIDefinition = "<font color='#ff9900'>Poor</font>";
                  } else if (parseInt(response.data['result'][0]['devices'][0]['AQI']) > 149 && parseInt(response.data['result'][0]['devices'][0]['AQI']) <= 200) {
                    AQIDefinition = "<font color='#ff6633'>Unhealthy</font>";
                  } else {
                    AQIDefinition = "<font color='#990073'>Polluted / Hazardous</font>";
                  }
                  document.getElementById("aqiresult").innerHTML = "<h1>Air Quality</h1><h2>Air Quality in room " + response.data['result'][0]['devices'][0]['devicelocation'] + "</h2><p><strong>" + response.data['result'][0]['devices'][0]['AQI'] + "</strong> AQI which is is currently rated <strong>\"" + AQIDefinition + "\"</strong> as per <a href='https://en.wikipedia.org/wiki/Air_quality_index'>definition</a> (Australia)</p><p><strong>Last updated:</strong> " + (new Date(response.data['result'][0]['devices'][0]['updateTS'])) + ")</p>";
                  callback({
                    fetched: true,
                    device: response.data['result'][0]['devices'][0]
                  });
                } else {
                  document.getElementById("aqiresult").innerHTML = "<h1>Air Quality</h1><h2>Air Quality not available</h2><p>The device has been offline for a long time or moved</p>";
                  console.log("Results attribute");
                  console.log(JSON.stringify(response.data['result'][0]));
                  callback({
                    reason: "Devices attribute doesnt exist",
                    fetched: false
                  });
                }
              } else {
                document.getElementById("aqiresult").innerHTML = "<h1>Air Quality</h1><h2>Air Quality not available</h2><p>The device has been offline for a long time or moved</p>";
                callback({
                  reason: "Device count should be 1 (Currently: " + response.data['result'].length + ")",
                  fetched: false
                });
              }
            } else {
              document.getElementById("aqiresult").innerHTML = "<h1>Air Quality</h1><h2>Unknown Error</h2><p>The backend is returning errorneus results</p>";
              callback({
                reason: "Result attribute doesn't exist",
                fetched: false
              });
            }
          } else {
            document.getElementById("aqiresult").innerHTML = "<h1>Air Quality</h1><h2>Unknown Error</h2><p>The backend is returning errorneus results</p>";
            callback({
              reason: "meta status is not 200",
              fetched: false
            });
          }
        } else {
          document.getElementById("aqiresult").innerHTML = "<h1>Air Quality</h1><h2>Unknown Error</h2><p>The backend is returning errorneus results</p>";
          callback({
            reason: "meta status does not exist",
            fetched: false
          });
        }
      } else {
        document.getElementById("aqiresult").innerHTML = "<h1>Air Quality</h1><h2>Unknown Error</h2><p>The backend is returning errorneus results</p>";
        callback({
          reason: "meta attribute does not exist",
          fetched: false
        });
      }
    } else {
      document.getElementById("aqiresult").innerHTML = "<h1>Air Quality</h1><h2>Unknown Error</h2><p>The backend is returning errorneus results</p>";
      callback({
        reason: "No response data",
        fetched: false
      });
    }
  });

}

// https://thingspeak.com/channels/1342504/fields/1/last.json?api_key=PK9HI8EHKTD5A2PE
const check_aqi_outside = (callback) => {
  axios.get("https://thingspeak.com/channels/1342504/fields/1/last.json?api_key=PK9HI8EHKTD5A2PE").then((response) => {
    if (!response.data) {
      if (response.data["field1"] !== undefined && response.data["field1"] !== null) {
        document.getElementById("outsideaqi").innerHTML = "<h2>AQI Outside</h2> <p><strong>AQI Level:</strong> " + response.data["field1"].toString() + " AQI</p>";
      } else {
        document.getElementById("outsideaqi").innerHTML = "<h2>AQI Outside</h2> <p>AQI cannot get AQI outside (unexpected server response)</p>";
      }
    } else {
      document.getElementById("outsideaqi").innerHTML = "<h2>AQI Outside</h2> <p>AQI cannot get AQI outside (unexpected server response)</p>";
    };
  });
}
