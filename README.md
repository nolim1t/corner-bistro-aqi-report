## About

This page shows the Air Quality in Corner Bistro, but can be taylored for another page.

This page is [open source](https://gitlab.com/nolim1t/corner-bistro-aqi-report) because it would make sense being so, and designed for gitlab pages.

Although we can also taylor this for github pages too.

## Notes

- [Air Quality Index definitions](https://en.wikipedia.org/wiki/Air_quality_index)
